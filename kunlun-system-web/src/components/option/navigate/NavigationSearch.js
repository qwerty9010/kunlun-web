import React, { Component } from 'react';
import {Form, Input, Button, Row, Col, AutoComplete, DatePicker} from 'antd';
import styles from './Navigation.less';
import index from '../../../pages/index.css';
import commonStyles from '../../../pages/index.css';

const FormItem = Form.Item;
const Option = AutoComplete.Option;

class NavigationSearch extends React.Component {

  formRef = React.createRef();

  render() {

    const { onSearch, onReset } = this.props;

    const handleSearch = () => {
      let fields = this.formRef.current.getFieldsValue();
      onSearch(fields)
    };

    const handleReset = () => {
      this.formRef.current.resetFields();
      onReset()
    };

    const formItemLayout = {
      labelCol: {span: 10},
      wrapperCol: {span: 14},
    };

    const iconStyle = {
      verticalAlign: "bottom",
      marginRight: "5px",
    };

    return (
      <div className={commonStyles.singleRowSearch}>
        <Form ref={this.formRef}>
          <Row className={index.formRowDiv}>
            <Col span={6}>
              <FormItem {...formItemLayout} label="导航菜单名" name={"name"}>
                <Input placeholder="请输入导航菜单名" size="default"/>
              </FormItem>
            </Col>
            <Col span={6}>
              <FormItem>
                <Button size="default" icon={<i className="ri-search-line" style={iconStyle}></i>} style={{marginLeft: "10px", border: "0px", background: window._THEMECOLOR_}} onClick={() => handleSearch()}>查询</Button>
                <Button type="default" size="default" icon={<i className="ri-restart-line" style={iconStyle}></i>} style={{marginLeft: "10px"}} onClick={() => handleReset()}>重置</Button>
              </FormItem>
            </Col>
          </Row>
        </Form>
      </div>
    );
  };
}

export default NavigationSearch;
