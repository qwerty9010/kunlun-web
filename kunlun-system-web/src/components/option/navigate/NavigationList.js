import React from 'react';
import styles from './Navigation.less';
import {Popconfirm, Table} from 'antd';
import moment from 'moment';
import { SortableContainer, SortableElement, SortableHandle } from 'react-sortable-hoc';

class NavigationList extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {

    const { navigationList, updateList, onEditColor } = this.props;

    const DragHandle = SortableHandle(() => <div style={{ cursor: 'grab', color: '#ec0425'}}><i className="ri-arrow-up-down-line" style={{ fontSize: "20px" }} /></div>);

    const SortableBody = SortableContainer((props) => (<tbody {...props} />));

    const onSortEnd = ({ oldIndex, newIndex }) => {

      debugger

      if (oldIndex != newIndex) {
        let newData = [], oldRecord = null;
        for (let i = 0; i < navigationList.length; i++) {
          const record = navigationList[i];
          if (oldIndex == i) {
            oldRecord = record;
          } else {
            newData.push(record);
          }
        }
        newData.splice(newIndex, 0, oldRecord);
        updateList(newData);
      }
    };

    const DraggableContainer = (props) => (
      <SortableBody
        useDragHandle
        disableAutoscroll
        helperClass={styles.rowDragging}
        onSortEnd={onSortEnd}
        {...props}
      />
    );

    const SortableItem = SortableElement((props) => (<tr {...props} />));

    const DraggableBodyRow = ({ className, style, ...restProps }) => {
      const index = navigationList.findIndex(x => x.menuId == restProps['data-row-key']);
      return <SortableItem index={index} {...restProps} />;
    };

    const columns = [
      { title: '调整顺序', dataIndex: 'menuId', key: 'menuId', width: '10%', className: styles.dragVisible,
        render: (text, record, index) => <DragHandle /> },
      { title: '序号', dataIndex: '', key: '', width: '10%', className: styles.dragVisible,
        render: (text, record, index) => <span>{ index + 1 }</span> },
      { title: '导航菜单名', dataIndex: 'name', key: 'name', width: '20%', className: styles.dragVisible, },
      { title: '图标', dataIndex: 'color', key: 'color', width: '20%',
        render: (text, record, index) => <span onClick={() => onEditColor(record)}><i className={record.icon} style={{ fontSize: "18px", color: record.color }} /></span> },
      { title: '创建时间', dataIndex: 'createTime', key: 'createTime', width: '15%',
        render: (text, record, index) => <span>{text ? moment(text).format("YYYY-MM-DD") : null}</span> },
    ];

    return (
      <div style={{marginTop: "15px"}}>
        <Table
          bordered
          className={ styles.listTable }
          columns={columns}
          dataSource={navigationList}
          pagination={false}
          scroll={{ y: window.innerHeight * 0.71 }}
          rowKey={"menuId"}
          components={{ body: { wrapper: DraggableContainer, row: DraggableBodyRow }}}
        />
      </div>)
  }
}

export default NavigationList;
