import React, { Component } from 'react';
import { Button, Segmented } from 'antd';

const NavigationToolbar = (props) => {

  const { addSave } = props;

  const iconStyle = {
    verticalAlign: "bottom",
    marginRight: "5px",
  };

  return (
    <div style={{marginTop: "15px", display: "flex", flexDirection: "row"}}>
      <div style={{flex: 1}}>
        <Button type="primary" size="default" icon={<i className="ri-save-3-line" style={iconStyle}></i>} onClick={addSave}>保存</Button>
      </div>
      <div style={{flex: 1, float: "right", display: "flex"}}>
        <span style={{flex: 1, padding: "5px 0px 0px 0px"}}>快捷导航数量：</span>
        <span style={{flex: 4}}>
          <Segmented block options={[8, 10, 12, 14]} />
        </span>
      </div>
    </div>
  );
};

export default NavigationToolbar;
