import React from 'react';
import { Table, Icon, Tooltip } from 'antd';
import config from '../../../config/config';
import styles from './Picture.less';
import moment from 'moment';

const PictureList = (props) => {

  const {
    pictureLoading, pictureList, rowSelection, onDownload, onView,
    currentPage, pageSize
  } = props;

  const iconStyle = {
    verticalAlign: "bottom",
    fontSize: "18px",
  }

  const columns = [
    { title: '序号', key: '', width: '5%',
      render: (text, record, index) => (index + 1) + (currentPage - 1) * pageSize, align: "center" },
    { title: '缩略图', dataIndex: 'serviceName', key: 'serviceName', width: '10%', align: "center",
      render: (text, record, index) => {
        const imgSrc = "data:" + record.type + ";base64," + record.base64;
        return (
          <span onClick={() => onView(record)}>
            <img width={50} height={50} src={imgSrc}  className={styles.pictureView} />
          </span>);
      }
    },
    { title: '文件名', dataIndex: 'name', key: 'name', width: '25%', align: "center" },
    { title: '类型', dataIndex: 'type', key: 'type', width: '10%', align: "center" },
    { title: '大小', dataIndex: 'size', key: 'size', width: '15%', align: "center",
      sorter: (x, y) => x.size - y.size },
    { title: '上传者', dataIndex: 'userName', key: 'userName', width: '10%', align: "center" },
    { title: '上传时间', dataIndex: 'createTime', key: 'createTime', width: '15%', align: "center",
      sorter: (x, y) => x.createTime - y.createTime, render: (text, record, index) => {
        return moment(text).format("yyyy-MM-DD HH:mm:ss");
      }
    },
    { title: '操作', key: 'operate', width: '10%', align: "center", render: (text, record, index) => (
      <a onClick={() => onDownload(record)}>
        <Tooltip title={"下載"}>
          <i className="ri-download-2-line" style={iconStyle}></i>
        </Tooltip>
      </a>)
    }];

  return (
    <div className={ styles.menuTable }>
      <Table
        size={"small"}
        columns={columns}
        dataSource={pictureList}
        bordered={true}
        loading={pictureLoading}
        rowSelection={rowSelection}
        rowKey={record => record.id}
        pagination={false}
        scroll={{y: (window.innerHeight - 195)}}
      />
    </div>
  );
};

export default PictureList;
