import React from 'react';
import {Row, Col, Spin, Tree, Drawer} from 'antd';
import styles from './AllotAuthorize.less';

const TreeNode = Tree.TreeNode;

class ViewAuthorizeDrawer extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      expandedTreeNodeKeys: []
    };
  }

  render() {

    const {
      radioValue, viewAuthorizeDrawerVisible, onClose, menuLimitLoading, authorizeDetailRecord
    } = this.props;
    const { checkedTreeNodeKeys, expandedTreeNodeKeys } = this.state;

    const onExpandTreeNode = (expandedTreeNodeKeys, item) => {
      this.setState({ expandedTreeNodeKeys })
    }

    const generateTreeNodes = (data) => data.map((item) => {
      if (item.children) {
        return (
          <TreeNode title={item.name} key={item.id} dataRef={item}>
            {generateTreeNodes(item.children)}
          </TreeNode>
        );
      }
      return <TreeNode title={item.name} key={item.id} dataRef={item}/>;
    })

    const initValue = (fieldName) => authorizeDetailRecord && authorizeDetailRecord[fieldName] ? authorizeDetailRecord[fieldName] : "";
    const treeHeight = (window.innerHeight - 95) + "px";
    const menuList = initValue("menuModels");
    const flag = "department" == radioValue;

    // 返回工具栏新增、批量删除按钮
    return (
      <div>
        {/*<Spin spinning={menuLimitLoading}>*/}
          <Drawer
            title={"查看详情"}
            width={flag ? 300 : 500}
            placement="right"
            onClose={onClose}
            maskClosable={false}
            visible={viewAuthorizeDrawerVisible}
            className={styles.menuDrawer}
          >
            <Row>
              <Col span={flag ? 24 : 12} className={styles.userDetailDiv}>
                <Row style={{marginLeft: "-9px"}}>
                  <span><span className={styles.detailDiv}></span>人员详情</span>
                </Row>
                <Row>
                  <span>角色名称：</span>
                  <span>{initValue("correlateName")}</span>
                </Row>
                <Row>
                  <span>用户名：</span>
                  <span>{initValue("userName")}</span>
                </Row>
                <Row>
                  <span>性别：</span>
                  <span>{initValue("sex")}</span>
                </Row>
                <Row>
                  <span>电话号码：</span>
                  <span>{initValue("phoneNumber")}</span>
                </Row>
                <Row>
                  <span>邮箱：</span>
                  <span>{initValue("email")}</span>
                </Row>
                <Row>
                  <span>操作时间：</span>
                  <span>{initValue("operateTime")}</span>
                </Row>
              </Col>
              <Col span={1} style={{display: flag ? "none" : "block", borderLeft: "1px solid #f0f0f0", margin: "0px 5px 0px 10px"}} />
              <Col span={10} style={{display: flag ? "none" : "block", lineHeight: "3"}}>
                <Row>
                  <span><span className={styles.detailDiv}></span>菜单权限</span>
                </Row>
                <Row style={{overflow: "scroll", height: treeHeight}}>
                  <Tree
                    autoExpandParent={true}
                    onExpand={onExpandTreeNode}
                  >
                    { menuList && menuList.length > 0 ? generateTreeNodes(menuList) : null }
                  </Tree>
                </Row>
              </Col>
            </Row>
          </Drawer>
        {/*</Spin>*/}
      </div>
    );
  };
}

export default ViewAuthorizeDrawer;
