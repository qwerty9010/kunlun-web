import React, { Component } from 'react';
import { Button } from 'antd';

const AllotAuthorizeToolbar = (props) => {

  const { onAuthorize, onAllot, radioValue } = props;

  const iconStyle = {
    verticalAlign: "bottom",
    marginRight: "5px",
  }

  let firstIcon = "ri-links-line", secondIcon = "ri-lock-2-line", firstButton = "人员分配", secondButton = "菜单授权";
  if ("department" == radioValue) {
    firstIcon = "ri-links-line";
    firstButton = "人员分配";
  } else if ("post" == radioValue) {
    firstIcon = "ri-user-follow-line";
    firstButton = "人员任命";
  } else {
    firstIcon = "ri-contacts-line";
    firstButton = "关联用户";
  }

  return (
    <div style={{marginTop: "15px"}}>
      <Button type="primary"
              size="default"
              icon={<i className={firstIcon} style={iconStyle}></i>}
              onClick={onAllot}
      >
        {firstButton}
      </Button>
      {
        "department" != radioValue &&
        <Button style={{marginLeft: "15px", color: "#FE9400", border: "1px solid #FE9400"}}
                icon={<i className={secondIcon} style={iconStyle}></i>}
                onClick={onAuthorize}
        >
          {secondButton}
        </Button>
      }
    </div>
  );
}

export default AllotAuthorizeToolbar;
