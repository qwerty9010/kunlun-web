import React from 'react';
import styles from './AllotAuthorize.less';
import {Table, Row, Col, Tag, Tooltip, Popconfirm} from 'antd';
import moment from 'moment';
import config from "@/config/config";

const AllotAuthorizeList = (props) => {

  const { radioValue, authorizeData, onDelete, onViewDetail, currentPage, pageSize } = props;

  const iconStyle = {
    verticalAlign: "bottom",
    marginRight: "5px",
  }

  const departmentColumns = [
    { title: '序号', width: '10%', render: (text, record, index) => <span>{(index + 1) + (currentPage - 1) * pageSize}</span> },
    { title: '用户名', dataIndex: 'userName', key: 'userName', width: '20%' },
    { title: '部门名称', dataIndex: 'correlateName', key: 'correlateName', width: '30%' },
    { title: '操作时间', dataIndex: 'operateTime', key: 'operateTime', width: '20%' },
    { title: '操作', dataIndex: 'operate', key: 'operate',
      render: (text, record, index) =>
        <Row>
          <Col span={12}>
            <Tooltip title={"删除人员"}>
              <Popconfirm
                title="确定删除数据？"
                onConfirm={() => onDelete(record)}
                onCancel={() => {}}
                okText="确定"
                cancelText="取消"
              >
                <a><i className="ri-delete-bin-2-fill" style={{color: 'red', fontSize: "17px"}} /></a>
              </Popconfirm>
            </Tooltip>
          </Col>
          <Col span={12}>
            <Tooltip title={"查看详情"}>
              <a onClick={() => onViewDetail(record)}>
                <i className="ri-file-text-fill" style={{color: '#13C2C2', fontSize: "17px"}} />
              </a>
            </Tooltip>
          </Col>
        </Row>
    }
  ];

  const postColumns = [
    { title: '序号', width: '10%', render: (text, record, index) => <span>{(index + 1) + (currentPage - 1) * pageSize}</span> },
    { title: '用户名', dataIndex: 'userName', key: 'userName', width: '15%' },
    { title: '岗位名称', dataIndex: 'correlateName', key: 'correlateName', width: '15%' },
    { title: '岗位权限项', dataIndex: 'authorizeItem', key: 'authorizeItem', width: '30%' },
    { title: '操作时间', dataIndex: 'operateTime', key: 'operateTime', width: '20%' },
    { title: '操作', dataIndex: 'operate', key: 'operate',
      render: (text, record, index) =>
        <Row>
          <Col span={12}>
            <Tooltip title={"删除人员"}>
              <Popconfirm
                title="确定删除数据？"
                onConfirm={() => onDelete(record)}
                onCancel={() => {}}
                okText="确定"
                cancelText="取消"
              >
                <a><i className="ri-delete-bin-2-fill" style={{color: 'red', fontSize: "17px"}} /></a>
              </Popconfirm>
            </Tooltip>
          </Col>
          <Col span={12}>
            <Tooltip title={"查看详情"}>
              <a onClick={() => onViewDetail(record)}>
                <i className="ri-file-text-fill" style={{color: '#13C2C2', fontSize: "17px"}} />
              </a>
            </Tooltip>
          </Col>
        </Row>
    }
  ];

  const roleColumns = [
    { title: '序号', width: '10%', render: (text, record, index) => <span>{(index + 1) + (currentPage - 1) * pageSize}</span> },
    { title: '用户名', dataIndex: 'userName', key: 'userName', width: '15%' },
    { title: '角色名称', dataIndex: 'correlateName', key: 'correlateName', width: '15%' },
    { title: '角色权限项', dataIndex: 'authorizeItem', key: 'authorizeItem', width: '30%' },
    { title: '操作时间', dataIndex: 'operateTime', key: 'operateTime', width: '20%' },
    { title: '操作', dataIndex: 'operate', key: 'operate',
      render: (text, record, index) =>
        <Row>
          <Col span={12}>
            <Tooltip title={"删除人员"}>
              <Popconfirm
                title="确定删除数据？"
                onConfirm={() => onDelete(record)}
                onCancel={() => {}}
                okText="确定"
                cancelText="取消"
              >
                <a><i className="ri-delete-bin-2-fill" style={{color: 'red', fontSize: "17px"}} /></a>
              </Popconfirm>
            </Tooltip>
          </Col>
          <Col span={12}>
            <Tooltip title={"查看详情"}>
              <a onClick={() => onViewDetail(record)}>
                <i className="ri-file-text-fill" style={{color: '#13C2C2', fontSize: "17px"}} />
              </a>
            </Tooltip>
          </Col>
        </Row>
    }
  ];
  const columns = "department" == radioValue ? departmentColumns : "post" == radioValue ? postColumns : roleColumns;

  return (
    <div className={ styles.listTable }>
      <Table
        bordered
        tableLayout={"auto"}
        columns={columns}
        dataSource={authorizeData}
        pagination={false}
        rowKey={record => record.id}
        scroll={{y: (window.innerHeight - 230)}}
      />
    </div>
  );
};

export default AllotAuthorizeList;
