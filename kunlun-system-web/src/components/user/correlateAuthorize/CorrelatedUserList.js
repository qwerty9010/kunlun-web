import React from 'react';
import styles from './CorrelateAuthorize.less';
import {Table, Tag, Tooltip, Popconfirm} from 'antd';
import moment from 'moment';
import config from "@/config/config";

const CorrelateAuthorizeList = (props) => {

  const { radioValue, departmentList, departmentLoading, rowSelection, onEdit, onDelete, currentPage, pageSize } = props;

  const departmentColumns = [
    { title: '序号', width: '10%', render: (text, record, index) => <span>{(index + 1) + (currentPage - 1) * pageSize}</span> },
    { title: '用户名', dataIndex: 'userName', key: 'userName', width: '25%' },
    { title: '部门名称', dataIndex: 'departmentCode', key: 'departmentCode', width: '40%' },
    { title: '操作时间', dataIndex: 'operateTime', key: 'operateTime', width: '25%' }
  ];

  const postColumns = [
    { title: '序号', width: '10%', render: (text, record, index) => <span>{(index + 1) + (currentPage - 1) * pageSize}</span> },
    { title: '用户名', dataIndex: 'userName', key: 'userName', width: '15%' },
    { title: '岗位名称', dataIndex: 'postName', key: 'postName', width: '20%' },
    { title: '岗位权限', dataIndex: 'departmentCode', key: 'departmentCode', width: '30%' },
    { title: '操作时间', dataIndex: 'operateTime', key: 'operateTime', width: '15%' }
  ];

  const roleColumns = [
    { title: '序号', width: '10%', render: (text, record, index) => <span>{(index + 1) + (currentPage - 1) * pageSize}</span> },
    { title: '用户名', dataIndex: 'userName', key: 'userName', width: '15%' },
    { title: '角色名称', dataIndex: 'roleName', key: 'roleName', width: '20%' },
    { title: '角色权限', dataIndex: 'status', key: 'status', width: '30%' },
    { title: '操作时间', dataIndex: 'operateTime', key: 'operateTime', width: '15%' }
  ];
  const columns = "department" == radioValue ? departmentColumns : "post" == radioValue ? postColumns : roleColumns;

  return (
    <div className={ styles.listTable }>
      <Table
        bordered
        tableLayout={"auto"}
        rowSelection={rowSelection}
        columns={columns}
        dataSource={departmentList}
        pagination={false}
        loading={departmentLoading}
        rowKey={record => record.id}
        scroll={{y: (window.innerHeight - 230)}}
      />
    </div>
  );
};

export default CorrelateAuthorizeList;
