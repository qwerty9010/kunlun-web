import React, { Component } from 'react';
import { Button } from 'antd';

const UserToolsBar = (props) => {

  const { onAuthorize, onAllot, radioValue } = props;

  const iconStyle = {
    verticalAlign: "bottom",
    marginRight: "5px",
  }

  let firstIcon = "ri-lock-2-line", secondIcon = "", firstButton = "菜单授权", secondButton = "";
  if ("department" == radioValue) {
    firstIcon = "ri-links-line";
    firstButton = "人员分配";
  } else if ("post" == radioValue) {
    secondIcon = "ri-user-follow-line";
    secondButton = "人员任命";
  } else {
    secondIcon = "ri-contacts-line";
    secondButton = "关联用户";
  }

  return (
    <div style={{marginTop: "15px"}}>
      <Button type="primary"
              size="default"
              icon={<i className={firstIcon} style={iconStyle}></i>}
              onClick={onAuthorize}
      >
        {firstButton}
      </Button>
      {
        "department" != radioValue &&
        <Button style={{marginLeft: "15px", color: "#FE9400", border: "1px solid #FE9400"}}
                icon={<i className={secondIcon} style={iconStyle}></i>}
                onClick={onAllot}
        >
          {secondButton}
        </Button>
      }
    </div>
  );
}

export default UserToolsBar;
