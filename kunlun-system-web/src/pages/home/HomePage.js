import React, {Component} from 'react';
import { connect } from 'umi';
import {Spin} from 'antd';
import FunctionNavigation from '../../components/home/FunctionNavigation';
import UserStatisticsChart from '../../components/home/UserStatisticsChart';
import NewsMessageChart from '../../components/home/NewsMessageChart';
import RedisInfoChart from '../../components/home/RedisInfoChart';
import MQBrokerChart from '../../components/home/MQBrokerChart';
import UserWorkbenchCard from '../../components/home/UserWorkbenchCard';
import TodaySchedule from '../../components/home/TodaySchedule';
import ServerMemoryDiskChart from '../../components/home/ServerMemoryDiskChart';
import ServerClusterChart from '../../components/home/ServerClusterChart';
import ServiceInvokeList from '../../components/home/ServiceInvokeList';
import config from "../../config/config";
import indexStyles from './homeIndex.less';
import * as commonUtil from '../../utils/commonUtil';
import moment from 'moment';
import styles from "../../components/home/Home.less";

class HomePage extends React.Component {

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    const { dispatch, homeModel } = this.props;
    setInterval(() => {
      const year = moment(new Date()).format("YYYY");
      dispatch({type: "homeModel/getUserCount", payload: {}});
      dispatch({type: "homeModel/onSelectYear", payload: {year}});
      dispatch({type: "homeModel/getRedisInfos", payload: {}});
      dispatch({type: "homeModel/getMessages", payload: {}});
      dispatch({type: "homeModel/queryServiceInvokes", payload: {}});
    }, 1000 * 60);
  }

  componentWillUnmount() {
    clearInterval();
  }

  render() {

    const { dispatch, homeModel } = this.props;

    const { userCounts, redisInfos, mqInfos, loading, scheduleData, scheduleIndex, scheduleTotal,
      serviceInvokes, userStatistics, selectedYear } = homeModel;

    const serviceInvokeListProps = {
      serviceInvokes,
      onShowDetail: (key) => {
        commonUtil.sendRequestToHome(true, key, null);
      },
    }

    const userInfoCardProps = {
      userCounts,
      onShowDetail: (key) => {
        commonUtil.sendRequestToHome(true, key, null);
      }
    }

    const functionNavigationProps = {
      onShowDetail: (key) => {
        commonUtil.sendRequestToHome(true, key, null);
      }
    }

    const userStatisticsChartProps = {
      userStatistics,
      selectedYear,
      onSelectYear: (year) => {
        dispatch({type: "homeModel/updateState", payload: {selectedYear: year}});
        dispatch({type: "homeModel/onSelectYear", payload: {year}});
      }
    }

    const newsMessageChartProps = {
      onShowDetail: (key) => {
        commonUtil.sendRequestToHome(true, key, null);
      },
    }

    const redisInfoChartProps = {
      redisInfos,
    }

    const mqNumberProps = {
      mqInfos,
      onShowDetail: (key) => {
        commonUtil.sendRequestToHome(true, key, null);
      },
    }

    const todayScheduleProps = {
      scheduleData,
      scheduleIndex,
      scheduleTotal,
      onShowDetail: (key) => {
        commonUtil.sendRequestToHome(true, key, null);
      },
      onClickArrow: (arrowType) => {
        dispatch({type: "homeModel/onClickArrow", payload: {arrowType}});
      }
    }

    return (
      <div className={indexStyles.home_layout_div}>
        <Spin spinning={loading} size={"large"} tip={"数据加载中。。。。。。"}>
          <div className={indexStyles.home_content_div}>
            <div className={indexStyles.work_bench_card_div}>
              {/* 工作台、用户统计 */}
              <UserWorkbenchCard {...userInfoCardProps} />
            </div>
            <div className={indexStyles.resource_statistics_show_div}>
              <div className={indexStyles.resource_show_div}>
                <div className={styles.commonDiv}>
                  {/* 便捷导航 */}
                  <FunctionNavigation {...functionNavigationProps} />
                </div>
                <div className={indexStyles.userMessageDiv}>
                  <div className={indexStyles.userChartMonthDiv}>
                    {/* 用户访问 */}
                    <UserStatisticsChart {...userStatisticsChartProps}/>
                  </div>
                  <div className={indexStyles.messageChartDiv}>
                    {/* 新闻通知 */}
                    <NewsMessageChart {...newsMessageChartProps} />
                  </div>
                </div>
                <div className={indexStyles.redisMQShowDiv}>
                  <div className={indexStyles.redisDiv}>
                    {/* MQ队列统计 */}
                    <RedisInfoChart {...redisInfoChartProps}/>
                  </div>
                  <div className={indexStyles.mqDiv}>
                    {/* Redis资源 */}
                    <MQBrokerChart {...mqNumberProps}/>
                  </div>
                </div>
              </div>
              <div className={indexStyles.resource_statistics_div}>
                <div className={styles.todayScheduleDiv}>
                  {/* 事项日程 */}
                  <TodaySchedule {...todayScheduleProps} />
                </div>
                <div className={indexStyles.tableADiv}>
                  {/* 服务器资源 */}
                  <ServerMemoryDiskChart />
                </div>
                <div className={indexStyles.tableBDiv}>
                  {/* 服务资源 */}
                  <ServerClusterChart />
                </div>
                <div className={indexStyles.tableCDiv}>
                  {/* 服务调用 */}
                  <ServiceInvokeList {...serviceInvokeListProps} />
                </div>
              </div>
            </div>
          </div>
          {/* Footer脚标 */}
          <div className={indexStyles.home_footer_Div}>
            <div className={indexStyles.footFontDiv}>{config.FOOTER_TEXT}</div>
          </div>
        </Spin>
      </div>
    );
  }
}

function mapStateToProps({ globalModel, homeModel }) {
  return { globalModel, homeModel };
}

export default connect(mapStateToProps)(HomePage);
